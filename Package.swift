// swift-tools-version:5.0
import PackageDescription

let package = Package(
    name: "Listar",
    products: [],
    dependencies: [
        .package(url: "https://github.com/Quick/Quick.git", .upToNextMajor(from: "2.0.0")),
        .package(url: "https://github.com/Quick/Nimble.git", .upToNextMajor(from: "8.0.1")),
    ],
    targets: [
        .testTarget(
            name: "ListarTests",
            dependencies: [
                "Quick",
                "Nimble"
            ]
        ),
        .testTarget(
            name: "FlistarTests",
            dependencies: [
                "Quick",
                "Nimble"
            ]
        ),
    ]
)
