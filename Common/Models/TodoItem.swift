//
//  TodoItem.swift
//  Listar
//
//  Created by Manuel Meyer on 01/04/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

struct TodoItem: Equatable, Hashable {
    
    enum Change {
        case text(String)
        case completed(Bool)
        case id(UUID)
        case due(Date?)
    }
    
    let text: String
    let completed: Bool
    let id: UUID
    let dueDate: Date?
    let alterDate: Date?
    let creationDate: Date

    init(text: String) {
        self.init(text, false, UUID(), nil, Date(), nil)
    }
    
    func alter(_ changes: Change...) -> TodoItem { changes.reduce(self) { $0.alter($1) } }
    
// MARK: - private
    
    private init (_ text: String, _ completed: Bool, _ id: UUID, _ dueDate:Date?, _ creationDate: Date, _ alterDate:Date?) {
        self.text = text
        self.completed = completed
        self.id = id
        self.creationDate = creationDate
        self.dueDate = dueDate
        self.alterDate = alterDate
    }
    
    private func alter(_ change:Change) -> TodoItem {
        let alterDate = Date()
        switch change {
        case .text     (let text     ): return TodoItem( text, completed, id, dueDate, creationDate, alterDate )
        case .completed(let completed): return TodoItem( text, completed, id, dueDate, creationDate, alterDate )
        case .id       (let id       ): return TodoItem( text, completed, id, dueDate, creationDate, alterDate )
        case .due      (let dueDate  ): return TodoItem( text, completed, id, dueDate, creationDate, alterDate )
        }
    }
}
