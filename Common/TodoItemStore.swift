//
//  TodoItemStore.swift
//  LibExample
//
//  Created by Manuel on 18/10/2019.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

final
class TodoItemStore {
    
    init(diskWriter: DiskWriting) {
        self.diskWriter = diskWriter
    }
    
    private let diskWriter: DiskWriting
    
    func add(todo: TodoItem) {
        write(todo: todo)
        list.append(todo)
    }
    func update(old: TodoItem, new: TodoItem) {
        if let idx = list.firstIndex(where: {$0.id == old.id }){
            if new.id != old.id {
                diskWriter.delete(todo: old) { _ in }
            }
            list[idx] = new
        }
        write(todo: new)
    }
    
    func delete(todo: TodoItem) {
        list = list.filter { $0.id != todo.id }
        diskWriter.delete(todo: todo) { _ in }
    }
    
    func fetchAll() -> [TodoItem] { list }
    var count: Int { list.count }
    
    private var list: [TodoItem] = []
    
    private func write(todo: TodoItem) {
        diskWriter.writeToDisk(todos: [todo]) { _ in }
    }
}
