//
//  Message.swift
//  Listar
//
//  Created by Manuel Meyer on 19/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

enum Message {
    case todo(TodoList)
    case application(Application)
    case state(State)
    
    enum TodoList {
        case item(Item)
        case all(All)
        
        enum Item {
            case add(TodoItem)
            case added(TodoItem)
            
            case complete(TodoItem)
            case completed(TodoItem)
            
            case update((old:TodoItem, new:TodoItem))
            case updated(TodoItem)
            
            case delete(TodoItem)
            case deleted(TodoItem)
            case deletionError(DeletionError)
        }
        
        enum All {
            case items(Items)
            
            enum Items {
                case load
                case errorLoading(Error)
                
                case fetch
                case fetched([TodoItem])
            }
        }
    }
    
    enum Application {
        case didBecomeActive
        case didEnterBackground
    }
    
    enum State {
        case loadState
    }
}
