//
//  FetchTodoItems.swift
//  Listar
//
//  Created by Manuel Meyer on 31/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct FetchTodoItems: UseCase {
    
    init(store: TodoItemStore, responseHandler: @escaping ((FetchTodoItems.Response) -> ())) {
        self.responseHandler = responseHandler
        self.store = store
    }
    
    private let responseHandler: ((Response) -> ())
    private let store: TodoItemStore
    
    func handle(request: Request) {
        switch request {
        case .all:
            responseHandler(.fetchedAll(store.fetchAll()))
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request {
        case all
    }
    
    enum Response {
        case fetchedAll([TodoItem])
    }
}
