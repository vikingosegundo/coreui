//
//  UpdateTodoItem.swift
//  Listar
//
//  Created by Manuel Meyer on 03/04/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct UpdateTodoItem: UseCase {
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request {
        case update((old: TodoItem, new: TodoItem))
    }
    
    enum Response {
        case updated(TodoItem)
    }
    
    init(store: TodoItemStore, responseHandler: @escaping ((Response) -> ())) {
        self.responseHandler = responseHandler
        self.store = store
    }
    
    private let responseHandler: ((Response) -> ())
    private let store: TodoItemStore
    
    func handle(request: Request) {
        switch request {
        case .update(let oldNew):
            store.update(old: oldNew.old, new: oldNew.new)
            responseHandler(.updated(oldNew.new))
        }
    }
}
