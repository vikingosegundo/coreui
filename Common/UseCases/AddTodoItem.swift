//
//  AddTodoItem.swift
//  Listar
//
//  Created by Manuel Meyer on 31/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

struct AddTodoItem: UseCase {
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request {
        case add(TodoItem)
    }
    
    enum Response {
        case added(TodoItem)
    }
    
    init(store: TodoItemStore, responseHandler: @escaping ((AddTodoItem.Response) -> ())) {
        self.responseHandler = responseHandler
        self.store = store
    }
    
    private let responseHandler: ((Response) -> ())
    private let store: TodoItemStore
    
    func handle(request: Request) {
        switch request {
        case .add(let todo):
            store.add(todo: todo)
            responseHandler(.added(todo))
        }
    }
}
