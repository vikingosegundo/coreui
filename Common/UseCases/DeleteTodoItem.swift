//
//  DeleteTodoItem.swift
//  Listar
//
//  Created by Manuel Meyer on 31/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//


enum DeletionError: Error, Equatable {
    case todoItemNotInStore(TodoItem)
}

struct DeleteTodoItem: UseCase {
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request  { case delete(TodoItem) }
    enum Response {
        case deleted(TodoItem)
        case error(DeletionError)
    }
    
    init(store: TodoItemStore, responseHandler: @escaping ((DeleteTodoItem.Response) -> ())) {
        self.responseHandler = responseHandler
        self.store = store
    }
    
    private let responseHandler: ((DeleteTodoItem.Response) -> ())
    private let store: TodoItemStore
    
    func handle(request: DeleteTodoItem.Request) {
        switch request {
        case .delete(let todo):
            let previousCount = store.count
            store.delete(todo: todo)
            if previousCount == store.count {
                responseHandler(.error(.todoItemNotInStore(todo)))
            }
            responseHandler(.deleted(todo))
        }
    }
}


struct LoadTodoItems: UseCase {
    typealias RequestType = Request
    typealias ResponseType = Response
    
    enum Request { case load }
    enum Response {
        case loaded([TodoItem])
        case error(Error)
    }
    
    init(diskReader: DiskReading, responseHandler: @escaping ((LoadTodoItems.Response) -> ())) {
        self.diskReader = diskReader
        self.responseHandler = responseHandler
    }
    
    private let responseHandler: ((LoadTodoItems.Response) -> ())
    private let diskReader: DiskReading
    
    func handle(request: Request) {
        switch request {
        case .load:
            diskReader.readFromDisk { result in
                switch result {
                case .success(let todos): responseHandler(.loaded(todos))
                case .failure(let error): responseHandler(.error(error))
                }
            }
        }
    }
}
