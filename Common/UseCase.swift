//
//  UseCase.swift
//  Listar
//
//  Created by Manuel Meyer on 19/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

protocol UseCase {
    associatedtype RequestType
    associatedtype ResponseType
    
    func handle(request: RequestType)
}
