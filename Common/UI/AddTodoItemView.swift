//
//  TodoItemEntryView.swift
//  Listar
//
//  Created by Manuel Meyer on 02/04/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct AddTodoItemView: View {
    init(todoCreated: @escaping (TodoItem) -> ()) {
        self.todoCreated = todoCreated
    }
    
    var body: some View {
        VStack {
            TextField("Enter Todo", text: $text)
                .padding()
            Button(action: saveTodo) {
                Text("Save Todo")
            }
        }
    }
    
    @State private var text: String = ""

    private let todoCreated: (TodoItem) -> ()
    private func saveTodo() {
        if text.count > 0 {
            todoCreated(TodoItem(text: text))
        }
    }
}

struct TodoItemEntryView_Previews: PreviewProvider {
    static var previews: some View {
        AddTodoItemView(todoCreated: { _ in })
    }
}
