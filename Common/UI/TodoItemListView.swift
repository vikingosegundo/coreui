//
//  TodoItemListView.swift
//  Listar
//
//  Created by Manuel Meyer on 02/04/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct TodoItemListView: View {
    
    let todo: TodoItem
    let toggle: (TodoItem) -> ()
    let select: (TodoItem) -> ()
    
    var body: some View {
        HStack {
            Text(todo.text)
                .strikethrough(todo.completed)
            Spacer()
            Text(todo.completed ? "✔️" :  "◯")
                .frame(width: 60, height: 60, alignment: .center)
                .onTapGesture { self.toggle(self.todo) }
        }
        .foregroundColor(todo.completed ? .gray : .black)
        .contentShape(Rectangle())
        .onTapGesture { self.select(self.todo) }
    }
}

struct TodoItemListView_Previews: PreviewProvider {
    static var previews: some View {
        TodoItemListView(todo: TodoItem(text: "hey Ho"), toggle: { _ in }, select: { _ in })
    }
}
