//
//  TodoItemEntryView.swift
//  Listar
//
//  Created by Manuel Meyer on 02/04/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import SwiftUI

struct TodoItemEditEntryView: View {
    init(todoCreated: @escaping (TodoItem) -> ()) {
        self.todoCreated = todoCreated
    }
    
    var body: some View {
        VStack {
            TextField("Enter Todo", text: $text)

            Button(action: saveTodo) {
                Text("Save Todo")
            }
        }
    }
    
    @State private var text: String = ""

    private let todoCreated: (TodoItem) -> ()
    private func saveTodo() {
        if text.count > 0 {
            todoCreated(.init(text: text, completed: false))
        }
    }
}

struct TodoItemEntryView_Previews: PreviewProvider {
    static var previews: some View {
        TodoItemEditEntryView(todoCreated: { _ in })
    }
}
