//
//  ContentView.swift
//  Listar
//
//  Created by Manuel Meyer on 19/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Combine
import SwiftUI

extension Sequence {
    func sorted<T: Comparable>(by keyPath: KeyPath<Element, T>) -> [Element] {
        return sorted { a, b in
            return a[keyPath: keyPath] < b[keyPath: keyPath]
        }
    }
}

fileprivate
class StateHolder: ObservableObject {
    let objectWillChange = ObservableObjectPublisher()
    var todos: [TodoItem] = []{ willSet { objectWillChange.send() } }
}

struct ContentView: View {
    
    init(handler: @escaping (Message) -> ()) {
        self.handler = handler
        self.state = StateHolder()
    }
    
    func handle(msg: Message) {
        let fetch = { self.handler(.todo(.all(.items(.fetch)))) }
        if case .todo(.item(.added(_)))   = msg { fetch() }
        if case .todo(.item(.updated(_))) = msg { fetch() }
        if case .todo(.item(.deleted(_))) = msg { fetch() }
        if case .todo(.all(.items(.fetched(let todos)))) = msg { state.todos = todos }
    }
    
    var body: some View {
        VStack {
            List {
                ForEach(state.todos.sorted(by: \.creationDate), id: \.self) {
                    TodoItemListView(todo: $0, toggle: self.toggle, select: { self.edit(todo: $0)})
                }.onDelete { self.deleteItem(at: $0) }
            }.sheet(isPresented: self.$showingEditTodoForm) {
                EditTodoItemView(todo: self.selectedTodo!) { self.update(todo: $0) }
            }
            
            Button(action: { self.showingAddTodoForm.toggle() }){
                Text("add todo")
            }.sheet(isPresented: $showingAddTodoForm) {
                AddTodoItemView { self.add(todo: $0) }
            }
        }
    }
    
    private let handler: (Message) -> ()
    @ObservedObject fileprivate var state: StateHolder
    @State private var showingAddTodoForm = false
    @State private var showingEditTodoForm = false
    @State private var selectedTodo: TodoItem?
    
    private func add(todo: TodoItem) {
        self.showingAddTodoForm = false
        self.handler(Message.todo(.item(.add(todo))))
    }
    
    private func edit(todo: TodoItem) {
        selectedTodo = todo
        self.showingEditTodoForm = true
    }
    
    private func update(todo: TodoItem) {
        self.showingEditTodoForm = false
        self.handler(.todo(.item(.update((old:selectedTodo!, new:todo)))))
    }
    
    private func deleteItem(at indexSet: IndexSet) {
        for idx in indexSet {
            handler(.todo(.item(.delete(state.todos[idx]))))
        }
    }
    
    private func toggle(todo: TodoItem) {
        handler(.todo(.item(.update((old:todo, new:todo.alter(.completed(!todo.completed)))))))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(handler: { _ in })
    }
}
