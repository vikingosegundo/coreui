//
//  EditTodoItemView.swift
//  Listar
//
//  Created by Manuel Meyer on 03/04/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation
import SwiftUI

struct EditTodoItemView: View {
    
    init(todo: TodoItem, updated: @escaping (TodoItem) -> ()) {
        self.todo = todo
        self.updated = updated
        self.text = todo.text
        self.id = todo.id
        self.completed = todo.completed
    }
    
    var body: some View {
        VStack {
            TextField("", text: $text)
                .onAppear { self.text = self.todo.text }
            Toggle(isOn: $completed){ Text("completed") }
                .onAppear { self.completed = self.todo.completed }
            Button(action: saveTodo) { Text("Save Todo") }
        }.padding()
    }
    
    @State private var text: String = ""
    @State private var completed: Bool = false
    private let todo: TodoItem
    private var id  : UUID!
    private let updated: (TodoItem) -> ()
    
    private func saveTodo() {
        guard text.count > 0 else { return }
        updated(TodoItem(text: text).alter(.completed(completed), .id(id)))
    }
}
struct EditTodoItemView_Previews: PreviewProvider {
    static var previews: some View {
        EditTodoItemView(todo: TodoItem(text: "s"), updated: { _ in })
    }
}
