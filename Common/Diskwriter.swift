//
//  Diskwriter.swift
//  Listar
//
//  Created by Manuel Meyer on 04/04/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

import Foundation

enum AppError: Error {
    case notImplemented(String)
}

enum PersistingError: Error {
    case errorWhilePersisting([TodoItem],Error)
}

enum DiskWritingError: Error {
    case errorWhileWriting([TodoItem], Error)
    case errorWhileDeleting(TodoItem, Error)
}

enum DiskReadingError: Error {
    case errorWhileReadingTodos(Error)
}

protocol DiskIO {
    init(pathBuilder: PathBuilding, fileManager: FileManager, dictionaryName: String)
}

protocol DiskWriting: DiskIO {
    func writeToDisk(todos: [TodoItem], result: (Result<[TodoItem], DiskWritingError>) -> ())
    func delete(todo: TodoItem, result: (Result<TodoItem, DiskWritingError>) -> ())
}

protocol DiskReading: DiskIO {
    func readFromDisk(result: (Result<[TodoItem], DiskReadingError>) -> ())
}

func asDictionary(todo: TodoItem) -> [String:Any] {
    return [
        "id": todo.id.uuidString,
        "text": todo.text,
        "completed": todo.completed
    ]
}

func asTodo(dict: [String: Any]) -> TodoItem? {
    let id = UUID(uuidString: dict["id"] as! String)!
    let text = dict["text"] as! String
    let completed = dict["completed"] as! Bool
    
    return TodoItem(text: text).alter(.id(id), .completed(completed))
}

extension FileManager {
    func directoryExists(at path: String) -> Bool {
        var isDir: ObjCBool = false
        let exists = fileExists(atPath: path, isDirectory:&isDir)
        return exists && isDir.boolValue
    }
}

struct DiskReader: DiskReading {
    init(pathBuilder: PathBuilding, fileManager: FileManager, dictionaryName: String) {
        self.pathBuilder = pathBuilder
        self.fileManager = fileManager
        self.dictionaryName = dictionaryName
    }
    
    private let pathBuilder: PathBuilding
    private let fileManager: FileManager
    private let dictionaryName: String

    func readFromDisk(result: (Result<[TodoItem], DiskReadingError>) -> ()) {
        do {
            let dir =  try pathBuilder.dictionaryInDocuments(named: dictionaryName, fileManger: fileManager)
            
            if !fileManager.directoryExists(at: dir.path) {
                try? fileManager.createDirectory(at: dir, withIntermediateDirectories: false, attributes: nil)
            }
            
            let filepaths = try fileManager.contentsOfDirectory(atPath: dir.path).filter { $0.hasSuffix(".todo") }
            
            let todos: [TodoItem] = try filepaths.compactMap { (path) in
                if let data = fileManager.contents(atPath: dir.appendingPathComponent(path).path) {
                    if let dict = try PropertyListSerialization.propertyList(from: data, options: [], format: nil) as? [String : Any] {
                        return asTodo(dict: dict)
                    }
                }
                return nil
            }

            result(.success(todos))
        } catch {
            result(.failure(.errorWhileReadingTodos(error)))
        }
    }
}

struct DiskWriter: DiskWriting {
    
    init(pathBuilder: PathBuilding, fileManager: FileManager, dictionaryName: String) {
        self.pathBuilder = pathBuilder
        self.fileManager = fileManager
        self.dictionaryName = dictionaryName
    }
    
    private let pathBuilder: PathBuilding
    private let fileManager: FileManager
    private let dictionaryName: String
    func writeToDisk(todos: [TodoItem], result: (Result<[TodoItem], DiskWritingError>) -> ()){
        
        do {
            let dir =  try pathBuilder.dictionaryInDocuments(named: dictionaryName, fileManger: fileManager)
            try todos.forEach { todo in
                let dict = asDictionary(todo: todo)
                let filePath =  dir.appendingPathComponent(todo.id.uuidString + ".todo")
                let data = try PropertyListSerialization.data(fromPropertyList: dict, format: .xml, options: 0)
                try data.write(to:filePath)
                result(.success(todos))
            }
        } catch {
            result(.failure(DiskWritingError.errorWhileWriting(todos, error)))
        }
    }
    
    func delete(todo: TodoItem, result: (Result<TodoItem, DiskWritingError>) -> ()) {
        do {
            let dir =  try pathBuilder.dictionaryInDocuments(named: dictionaryName, fileManger: fileManager)
            let filePath =  dir.appendingPathComponent(todo.id.uuidString + ".todo").path
            try fileManager.removeItem(atPath: filePath)
            result(.success(todo))
        } catch  {
            result(.failure(DiskWritingError.errorWhileDeleting(todo, error)))
        }
    }
}

