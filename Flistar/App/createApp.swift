//
//  createApp.swift
//  dareyou
//
//  Created by Manuel on 01/02/2020.
//  Copyright © 2020 Manuel. All rights reserved.
//

import Foundation

typealias  Input = (Message) -> ()
typealias Output = (Message) -> ()

func createApp(pathBuilder: PathBuilding,
                 receivers: [Input],
               rootHandler: @escaping Output) -> Input
{
    let store = TodoItemStore(
        diskWriter: DiskWriter(pathBuilder: pathBuilder,
                               fileManager: FileManager.default,
                            dictionaryName: "todos")
    )
    let features:[Input] = [
        todoListFeature(store: store,
                   diskReader: DiskReader(pathBuilder: pathBuilder,
                                          fileManager: FileManager.default,
                                       dictionaryName: "todos"),
                       output: rootHandler
        )
    ]
    return { msg in (receivers + features).forEach { $0(msg) } }
}
