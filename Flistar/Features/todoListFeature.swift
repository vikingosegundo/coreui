//
//  todoListFeature.swift
//  Listar
//
//  Created by Manuel Meyer on 02/04/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

func todoListFeature(store: TodoItemStore, diskReader: DiskReading, output: @escaping Output) -> Input {

    let loadTodos = LoadTodoItems(diskReader: diskReader) { (response) in
        switch response {
        case .loaded(let todos): todos.forEach { store.add(todo: $0) }; output(.todo(.all(.items(.fetch              ))))
        case  .error(let error):                                        output(.todo(.all(.items(.errorLoading(error)))))
        }
    }
    
    let fetchTodos = FetchTodoItems(store: store) { (response) in
        switch response {
        case .fetchedAll(let todos): output(.todo(.all(.items(.fetched(todos)))))
        }
    }

    let addTodo = AddTodoItem(store: store) { (response) in
        switch response {
        case .added(let todo): output(.todo(.item(.added(todo))))
        }
    }

    let updateTodo = UpdateTodoItem(store: store) { (response) in
        switch response {
        case .updated(let todo): output(.todo(.item(.updated(todo))))
        }
    }

    let deleteTodo = DeleteTodoItem(store: store){ (response) in
        switch response {
        case .deleted(let  todo): output(.todo(.item(.deleted(todo))))
        case   .error(let error): output(.todo(.item(.deletionError(error))))
        }
    }

    return { msg in
        if case .todo(.all(.items( .load)      )) = msg {  loadTodos.handle(request: .load            ) }
        if case .todo(.all(.items(.fetch)      )) = msg { fetchTodos.handle(request: .all             ) }
        if case .todo(.item(   .add(let todo)  )) = msg {    addTodo.handle(request: .add    (todo)   ) }
        if case .todo(.item(.update(let oldNew))) = msg { updateTodo.handle(request: .update (oldNew) ) }
        if case .todo(.item(.delete(let todo)  )) = msg { deleteTodo.handle(request: .delete (todo)   ) }
    }
}
