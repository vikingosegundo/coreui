//
//  AppDelegate.swift
//  Flistar
//
//  Created by Manuel Meyer on 31/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    var app: ((Message) -> ())!

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let ui = UI { self.app($0) }
        window = ui.window
        
        let receivers = [ui.handle(msg: )]
        app = createApp(pathBuilder: PathBuilder(), receivers: receivers) { self.app($0) }
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        app(.application(.didBecomeActive))
        app(.todo(.all(.items(.load))))
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        app(.application(.didEnterBackground))
    }
}

