//
//  ListarTests.swift
//  ListarTests
//
//  Created by Manuel Meyer on 29/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Quick
import Nimble
@testable import Listar

class Mock {
    class MessageHandler: MessageHandling, MessageSubscriber {
        
        init(callBack: @escaping (Message) -> ()) {
            self.callBack = callBack
        }
        
        let callBack: (Message) -> ()
        
        func handle(msg: Message) { callBack(msg) }
    }
}

class ListarTests: QuickSpec {
    override func spec() {
        
        var app: TodoApp!
        
        describe("Todo Feature"){
            var messageHandler: Mock.MessageHandler!
            var fetchedTodos: [TodoItem]!
            var deletionError: DeletionError?
            
            var todo:TodoItem!
            var todos: [TodoItem]!
            var tomorrow: Date?

            beforeEach {
                let fetch = { app.handle(msg: .todo(.all(.items(.fetch)))) }
                messageHandler = Mock.MessageHandler {
                    if case .todo(.all(.items(.fetched(let items)))) = $0 { fetchedTodos = items           }
                    if case .todo(.item(.added(_)))                  = $0 {                        fetch() }
                    if case .todo(.item(.updated(_)))                = $0 {                        fetch() }
                    if case .todo(.item(.deleted(_)))                = $0 {                        fetch() }
                    if case .todo(.item(.deletionError(let error)))  = $0 { deletionError = error; fetch() }
                }
                
                let cal = Calendar.current
                let todayInterval = cal.dateInterval(of: .day, for: Date())!
                tomorrow = cal.date(byAdding: .day, value: 1, to: todayInterval.start)
                
                app = TodoApp(pathBuilder: PathBuilder())
                app.add(subscriber: messageHandler)
                
                todo = TodoItem(text: "Hey Ho")
                todos = (1..<4).reduce([]) { $0 + [TodoItem(text: "\($1)")] }
                todos.forEach { app.handle(msg: .todo(.item(.add($0)))) }
            }
            afterEach { todos = nil; todo = nil; deletionError = nil; fetchedTodos = nil; messageHandler = nil; app = nil }
            
            context("adding"){
                it("adds a todo item") {
                    app.handle(msg: .todo(.item(.add(todo))))
                    
                    expect(fetchedTodos.last) == todo
                }
            }
            context("updating"){
                it("updates completion state for todo to true") {
                    app.handle(msg: .todo(.item(.update((old:todos[0], new:todos[0].alter(.completed(true)))))))
                    
                    expect(fetchedTodos[0].completed) == true
                }
                it("updates completion state for todo to false") {
                    app.handle(msg: .todo(.item(.update((old:todos[0], new:todos[0].alter(.completed(true)))))))
                    app.handle(msg: .todo(.item(.update((old:todos[0], new:todos[0].alter(.completed(false)))))))
                    
                    expect(fetchedTodos[0].completed) == false
                }
                it("updates text") {
                    app.handle(msg: .todo(.item(.update((old:todos[1], new:todos[1].alter(.text("Hello from the tests")))))))
                    
                    expect(fetchedTodos[1].text) == "Hello from the tests"
                }
                it("updates uuid"){
                    let uuid = UUID()
                    app.handle(msg: .todo(.item(.update((old:todos[2], new:todos[2].alter(.id(uuid)))))))
                    
                    expect(fetchedTodos[2].id) == uuid
                    
                }
                it("updates due date") {
                    app.handle(msg: .todo(.item(.update((old: todos[1], new: todos[1].alter(.due(tomorrow)))))))
                    
                    expect(fetchedTodos[1].dueDate) == tomorrow
                }
                it("changes several properties") {
                    let uuid = UUID()
                    let txt = "Hello from the tests"
                    let c = true
                    app.handle(msg: .todo(.item(.update((old:todos[2], new:todos[2].alter(.id(uuid), .completed(c), .text(txt), .due(tomorrow) ))))))
                    
                    expect(fetchedTodos[2].id       ) == uuid
                    expect(fetchedTodos[2].completed) == c
                    expect(fetchedTodos[2].text     ) == txt
                    expect(fetchedTodos[2].dueDate  ) == tomorrow
                }
                context("several changes to one proberty will be applied in order") {
                    let txt0 = "Hello from the tests"
                    let txt1 = "Is that a song by Adele?"
                    let txt2 = "No, that is «Hello from the other side»"
                    
                    it("updated text once") {
                        app.handle(msg: .todo(.item(.update((old:todos[2], new:todos[2].alter(.text(txt0)))))))
                        
                        expect(fetchedTodos[2].text) == txt0
                    }
                    it("updated text twice") {
                        app.handle(msg: .todo(.item(.update((old:todos[2], new:todos[2].alter(.text(txt0), .text(txt1)))))))
                        
                        expect(fetchedTodos[2].text) == txt1
                    }
                    it("updated text 3 times") {
                        app.handle(msg: .todo(.item(.update((old:todos[2], new:todos[2].alter(.text(txt0), .text(txt1), .text(txt2)))))))
                        
                        expect(fetchedTodos[2].text) == txt2
                    }
                }
            }
            context("fetching"){
                it("fetches all items") {
                    app.handle(msg: .todo(.all(.items(.fetch))))
                    
                    expect(fetchedTodos) == todos
                }
            }
            context("deleting") {
                beforeEach {
                    todo = TodoItem(text: "not in list")
                }
                
                it("deletes 1st todo from store") {
                    app.handle(msg: .todo(.item(.delete(todos[0]))))
                    
                    expect(fetchedTodos) == [todos[1], todos[2]]
                }
                it("deletes 2nd todo from store") {
                    app.handle(msg: .todo(.item(.delete(todos[1]))))
                    
                    expect(fetchedTodos) == [todos[0], todos[2]]
                }
                it("deletes 3rd todo from store") {
                    app.handle(msg: .todo(.item(.delete(todos[2]))))
                    
                    expect(fetchedTodos) == [todos[0], todos[1]]
                }
                it("gives an error for deleting a item that isnt in store") {
                    app.handle(msg: .todo(.item(.delete(todo))))
                    
                    expect(deletionError) == .todoItemNotInStore(todo)
                }
            }
        }
    }
}
