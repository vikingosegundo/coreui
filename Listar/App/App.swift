//
//  App.swift
//  LibExample
//
//  Created by Manuel on 07/01/2020.
// Copyright © 2020 Manuel Meyer. All rights reserved.
//

import Foundation

protocol MessageHandling: class {
    func handle(msg: Message)
}

protocol ResponseHandling {
    var responseHandler: MessageHandling? { get set }
}

protocol MessageProvider {
    func add(subscriber:MessageSubscriber)
}

protocol MessageSubscriber: MessageHandling { }

protocol App: MessageHandling { }

protocol SubscribableApp: App, MessageProvider {}


final
class TodoApp: SubscribableApp {
    init(pathBuilder: PathBuilding) {
        self.pathBuilder = pathBuilder
    }
    
    private let dirName = "todos"
    private let pathBuilder: PathBuilding
    private var subscribers: [MessageHandling] = []
    private lazy var features: [Feature] = [
        TodoListFeature(with: self,
                       store: TodoItemStore(diskWriter: DiskWriter(pathBuilder: pathBuilder,
                                                                   fileManager: FileManager.default,
                                                                dictionaryName: dirName
                                                        )
                              ),
                  diskReader: DiskReader(pathBuilder: pathBuilder,
                                         fileManager: FileManager.default,
                                      dictionaryName: dirName
                              )
        )
    ]

    func handle(msg: Message) {
        subscribers.forEach { $0.handle(msg: msg) }
        features.forEach { $0.handle(msg: msg) }
    }
    
    func add(subscriber: MessageSubscriber) { subscribers.append(subscriber) }
}
