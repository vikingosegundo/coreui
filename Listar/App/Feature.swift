//
//  Feature.swift
//  LibExample
//
//  Created by Manuel on 08/10/2019.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

protocol Feature {
    func handle(msg: Message)
}
