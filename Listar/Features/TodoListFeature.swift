//
//  TodoListFeature.swift
//  Listar
//
//  Created by Manuel Meyer on 31/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

final
class TodoListFeature: Feature {
    init(with rootMessageHandler: MessageHandling, store: TodoItemStore, diskReader: DiskReading) {
        self.rootMessageHandler = rootMessageHandler
        self.store = store
        self.diskReader = diskReader
    }
    
    func handle(msg: Message) { routeMessageToUseCases(msg) }

    private var rootMessageHandler: MessageHandling
    private let store: TodoItemStore
    private let diskReader: DiskReading
    
    // useCases
    private lazy var      loadTodos =   LoadTodoItems(diskReader: diskReader) { self.handle(response: $0) }
    private lazy var fetchTodoItems =  FetchTodoItems(     store: store     ) { self.handle(response: $0) }
    private lazy var    addTodoItem =     AddTodoItem(     store: store     ) { self.handle(response: $0) }
    private lazy var deleteTodoItem =  DeleteTodoItem(     store: store     ) { self.handle(response: $0) }
    private lazy var updateTodoItem =  UpdateTodoItem(     store: store     ) { self.handle(response: $0) }
    
    private func routeMessageToUseCases(_ msg: Message) {
        if case .todo(.all(.items( .load)      )) = msg { self.loadTodos     .handle(request: .load           ) }
        if case .todo(.item(.add(let todo)))      = msg { self.addTodoItem   .handle(request: .add(todo)      ) }
        if case .todo(.item(.update(let oldNew))) = msg { self.updateTodoItem.handle(request: .update(oldNew) ) }
        if case .todo(.all (.items(.fetch)))      = msg { self.fetchTodoItems.handle(request: .all            ) }
        if case .todo(.item(.delete(let todo)))   = msg { self.deleteTodoItem.handle(request: .delete(todo)   ) }
    }
    
    private func handle(response: LoadTodoItems.Response) {
        switch response {
        case .loaded(let todos): todos.forEach { store.add(todo: $0)}; rootMessageHandler.handle(msg: .todo(.all(.items(.fetch              ))))
        case  .error(let error):                                       rootMessageHandler.handle(msg: .todo(.all(.items(.errorLoading(error)))))
        }
    }
    
    private func handle(response: AddTodoItem.Response) {
        switch response {
        case .added(let todo): rootMessageHandler.handle(msg: .todo(.item(.added(todo))))
        }
    }
    
    private func handle(response: UpdateTodoItem.Response) {
        switch response {
        case .updated(let todo): rootMessageHandler.handle(msg: .todo(.item(.updated(todo))))
        }
    }
    
    private func handle(response: FetchTodoItems.Response) {
        switch response {
        case .fetchedAll(let items): rootMessageHandler.handle(msg: .todo(.all(.items(.fetched(items)))))
        }
    }
    
    private func handle(response: DeleteTodoItem.Response) {
        switch response {
        case .deleted(let todo):
            rootMessageHandler.handle(msg: .todo(.item(.deleted(todo))))
        case .error(.todoItemNotInStore(let todo)):
            rootMessageHandler.handle(msg:.todo(.item(.deletionError(DeletionError.todoItemNotInStore(todo)))))
        }
    }
}
