//
//  AppDelegate.swift
//  Listar
//
//  Created by Manuel Meyer on 19/03/2020.
//  Copyright © 2020 Manuel Meyer. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

     var window: UIWindow?
     var app: TodoApp!


     func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
         let ui = UI {
            self.app.handle(msg: $0)
         }
         window = ui.window
         
        app = TodoApp(pathBuilder: PathBuilder())
        app.add(subscriber: ui)
         
         return true
     }
     
     func applicationDidBecomeActive(_ application: UIApplication) {
        app.handle(msg: .application(.didBecomeActive))
        app.handle(msg: .todo(.all(.items(.load))))
     }

    func applicationDidEnterBackground(_ application: UIApplication) {
        app.handle(msg: .application(.didEnterBackground))
    }
}

extension UI: MessageSubscriber {}
